let rows = document.getElementById("rows");
let cols = document.getElementById("cols");
let runButton = document.getElementById("run");
let tableContainer = document.getElementById("tableContainer");

function buildTable() {
    let table = "<table>";
    for (let i = 1; i <= Number(rows.value); i++) {
        table += "<tr>";

        for (let j = 1; j <= Number(cols.value); j++) {
            table += "<td></td>";
        }

        table += "</tr>"
    }

    table += "</table>";

    tableContainer.innerHTML = table;
}

runButton.onclick = function() {
    buildTable();
}